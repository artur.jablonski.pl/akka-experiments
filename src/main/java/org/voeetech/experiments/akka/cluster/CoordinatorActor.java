package org.voeetech.experiments.akka.cluster;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import akka.routing.FromConfig;
import akka.routing.GetRoutees;
import akka.routing.Routees;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CoordinatorActor extends AbstractLoggingActor
{
    private static final Logger
        logger = LoggerFactory.getLogger(CoordinatorActor.class);

    @Override
    public void preStart()
    {
        ActorRef router =
            getContext()
                .actorOf(Props.empty().withRouter(FromConfig.getInstance()),
                         "router1");

        router.tell(GetRoutees.getInstance(), self());

    }

    @Override
    public Receive createReceive()
    {
        return
            ReceiveBuilder.create()
                          .match(Routees.class, r -> {
                              int noOfRoutees = r.getRoutees().size();
                              log().info("No of routees: {}", noOfRoutees);
                              if (noOfRoutees < 3)
                                  getSender().tell(GetRoutees.getInstance(),
                                                   getSelf());
                              else
                                  distributeMessages(getSender());
                          })
                          .matchAny(o -> log().info(
                              "oops, I don't understand this message {}",
                              o.getClass()))
                          .build();
    }

    private void distributeMessages(ActorRef router)
    {
        for (int i = 0; i < 20; i++) {
            WorkerActor.Message message =
                WorkerActor.Message.addAccount(
                    WorkerActor.Account.of("username_" + i,
                                           "password_" + i)
                );

            router.tell(message, ActorRef.noSender());
        }

    }

    public static Props props()
    {
        return
            Props.create(CoordinatorActor.class);
    }

}
