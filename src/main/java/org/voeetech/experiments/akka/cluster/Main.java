package org.voeetech.experiments.akka.cluster;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import akka.cluster.Cluster;
import akka.cluster.singleton.ClusterSingletonManager;
import akka.cluster.singleton.ClusterSingletonManagerSettings;
import akka.cluster.singleton.ClusterSingletonProxy;
import akka.cluster.singleton.ClusterSingletonProxySettings;
import akka.util.Timeout;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Main
{
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String... args)
        throws IOException

    {
        Config cfg = ConfigFactory.load();

        ActorSystem actorSystem =
            ActorSystem.create("ClusterSystem", cfg);

        ActorRef workerRef =
            actorSystem.actorOf(WorkerActor.props(), "worker");

        logger.info(workerRef.path().toStringWithoutAddress());

        Cluster.get(actorSystem).registerOnMemberUp((Runnable) () -> {

            final ClusterSingletonManagerSettings settings =
                ClusterSingletonManagerSettings.create(actorSystem);

            actorSystem.actorOf(
                ClusterSingletonManager.props(
                    CoordinatorActor.props(),
                    PoisonPill.getInstance(),
                    settings),
                "coordinator");

            ClusterSingletonProxySettings proxySettings =
                ClusterSingletonProxySettings.create(actorSystem);

            ActorRef proxy =
                actorSystem.actorOf(ClusterSingletonProxy
                                        .props("/user/coordinator",
                                               proxySettings),
                                    "coordinatorProxy");


        });

    }
}
