package org.voeetech.experiments.akka.cluster;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WorkerActor extends AbstractLoggingActor
{
    private List<Account> activeAccounts = new ArrayList<>();

    @Override
    public Receive createReceive()
    {
        return
            ReceiveBuilder.create()
                          .match(Message.class,
                                 m -> m.getType() ==
                                      Message.Type.ADD_ACCOUNT,
                                 m -> addAccount(m.getAccount()))
                          .matchAny(o -> log().info("oops, I don't understand this message {}", o.getClass()))
                          .build();
    }

    static class Account implements Serializable
    {
        private String userName;
        private String password;

        private Account() {}

        public static Account of(String userName, String password)
        {
            Account account = new Account();
            account.userName = userName;
            account.password = password;
            return account;
        }

        public String getUserName() {return userName;}

        public String getPassword() {return password;}

    }

    //protocol
    static class Message implements Serializable
    {
        enum Type
        {
            ADD_ACCOUNT
        }

        Type type;
        Account account;

        private Message() {}

        public static Message addAccount(Account account)
        {
            Message message = new Message();
            message.type = Type.ADD_ACCOUNT;
            message.account = account;
            return message;
        }

        public Type getType() { return type; }

        public Account getAccount() {return account;}
    }

    private void addAccount(Account account)
    {
        log().info("Adding account for user [{}]", account.getUserName());
        activeAccounts.add(account);
    }

    public static Props props()
    {
        return
            Props.create(WorkerActor.class);
    }

}
